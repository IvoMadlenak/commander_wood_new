<?php

/**
 * Project config
 */

define('DATETIME_FORMAT', 'Y-m-d H:i:s');

return [
    'displayErrorDetails' => true,
    'database'     => [
        'host'     => '',
        'port'     => 3306,
        'user'     => 'username',
        'password' => 'password',
        'dbName'   => '',
        'build'    => false
    ],

    // Monolog settings
    'logger' => require(__DIR__ . '/partial/logger.settings.php'),
];
