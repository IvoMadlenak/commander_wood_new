<?php

/**
 * Production project config
 */

ini_set('display_errors', false);
error_reporting(E_ALL);

return [
    'displayErrorDetails' => true,
    'database'     => [
        'host'     => '82.208.49.59', //db master
        'port'     => '3306',
        'user'     => 'mobiledispatcher',
        'password' => 'Hr3ZmafKXjCFn5BC',
        'dbName'   => 'mobiledispatcher',
        'build'    => false,
    ],

    // Monolog settings
    'logger' => require(__DIR__ . '/partial/logger.settings.php'),
];