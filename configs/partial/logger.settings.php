<?php

/**
 * error handler and logger settings
 *  - do not include in the main configuration file - better to keep it separately
 */

return [
    'basename' => 'coreapi_v3',
    'loglevel' => Psr\Log\LogLevel::DEBUG,
    'handlers' => [
        'stream' => [
            'path' => PROJECT_DIR . '/data/logs/error.log',
            // the default output format is "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
            'format' => "%datetime% : %level_name% > %message%\nContext: %context%\nExtra: %extra%\n---\n",
            'enabled' => true,
        ],
        'gelf' => [
            'host' => 'log.commander-systems.com',
            'port' => 12201,
            'enabled' => false,
        ],
    ],
];
