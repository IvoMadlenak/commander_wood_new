<?php

/**
 * Project config
 */

ini_set('display_errors', '1');
error_reporting(E_ALL);



return [
    'displayErrorDetails' => true,
    'database'     => [
        'host'     => 'db',
        'port'     => '3306',
        'user'     => 'root',
        'password' => 'root',
        'dbName'   => 'mobiledispatcher',
        'build'    => true
    ],

    // Monolog settings
    'logger' => require(__DIR__ . '/partial/logger.settings.php'),
];