<?php

ini_set('display_errors', true);
error_reporting(E_ALL);

define('PROJECT_DIR', dirname(__DIR__));

try {
    //boostrap app
    $container = require PROJECT_DIR . '/app/bootstrap.php';

    $app = $container->get(\Slim\App::class);

    $app->run();

} catch (\Throwable $e) {

    if (isset($errorHandler)) {
        $errorHandler->getLogger()->addError((string) $e);
    } else {
        error_log((string) $e, null, PROJECT_DIR . '/data/logs/error.log' );
    }
}