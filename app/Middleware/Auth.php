<?php
/**
 * Created by PhpStorm.
 * User: Pista
 * Date: 08.11.2018
 * Time: 9:20
 */

namespace App\Middleware;

use App\Core\Database;
use Psr\Container\ContainerInterface;

class Auth
{
    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */

    private $auth_token;
    private $device_id;
    private $anythingWrong;
    private $db;

    public function __construct(ContainerInterface $container)
    {
        $connection = $container->get(Database::class);
        $this->db = $connection;
    }

    /**
     * @param $request
     * @param $response
     * @param $next
     * @return mixed
     */
    public function __invoke($request, $response, $next)
    {
        $newResponse = $response;
        //$queryResult = array();
        /** @var object $request */

        if ($request->isGet() || $request->isDelete() || $request->isPost() || $request->isHeader()) {
            
            $a=$request->getHeaders();//posielanie cez header
            if ( isset($a['HTTP_AUTHTOKEN']) && !empty($a['HTTP_AUTHTOKEN']) && isset($a['HTTP_DEVICEID']) && !empty($a['HTTP_DEVICEID'])) {
                $this->auth_token = implode(", ",$a['HTTP_AUTHTOKEN']);
                $this->device_id = implode(", ",$a['HTTP_DEVICEID']);
                
                $now_datee=date('Y-m-d H:i:s');
                $queryResult = $this->db
                ->select()
                ->from('user_login_new')
                ->where('device_id', '=' , $this->device_id)
                ->where('auth_token', '=' , $this->auth_token)
                //->where('auth_token_time', '>=' , $now_datee)
                ->execute();

                /** @var object $queryResult */
                $queryResult = $queryResult->fetchAll();
                if ($queryResult && is_array($queryResult) && count($queryResult)) {
                    //$now_datee=date('Y-m-d H:i:s');
                    if ($queryResult[0]['auth_token_time']<=$now_datee) {
                        $this->anythingWrong = 4;
                        //$newResponse = $response->withStatus(403);
                        $newResponse->getBody()->write('Invalid authtoken. Please actual authtoken with help refresh token');
                    }else{
                        $this->anythingWrong = 777;
                    }
                }else{
                    $this->anythingWrong = 3;
                    //$newResponse = $response->withStatus(403);
                    $newResponse->getBody()->write('Invalid authentification.');
                }
            }
            else {
                $this->anythingWrong = 1;
            }
        }
        else {
            $this->anythingWrong = 2;
        }

        if ($this->anythingWrong && $this->anythingWrong != 777) {
            $newResponse = $response->withStatus(400);
            $newResponse->getBody()->write('Bad request (' . $this->anythingWrong . ').');
        }
        else {
            if(!isset($queryResult)){
                $queryResult = array();
            }
            $newResponse = $next($request->withAttribute('clientData', $queryResult), $newResponse);
        }

        return $newResponse;
    }
}