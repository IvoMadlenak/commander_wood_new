<?php

namespace App\Core;

use Slim\PDO;

final class Database extends PDO\Database
{
    /** @var PDO\Statement */
    private $stmt;

    /** @var array $settings */
    private $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;

        $port = isset($this->settings['port']) && $this->settings['port'] ? ';port=' . $this->settings['port'] : '';
        $dsn  = 'mysql:host=' . $this->settings['host'] . $port . ';charset=utf8;dbname=' . $this->settings['dbName'];

        parent::__construct($dsn, $this->settings['user'], $this->settings['password']);
    }

    public function getSettings(): array
    {
        return $this->settings;
    }
}
