<?php
/**
 * Created by PhpStorm.
 * User: adame
 * Date: 11. 6. 2018
 * Time: 13:17
 */

namespace App\Core;

use Exception\ConfigurationException;

final class Settings
{
    /**
     * @var array
     */
    private static $settings;

    /**
     * @var string|null
     */
    private $type;

    public function __construct(string $type = null)
    {
        $this->type = $type;

        self::$settings = array_merge(
            require PROJECT_DIR . '/configs/settings.global.php', //global settings
            require PROJECT_DIR . '/configs/settings.local.php' //environment specific configuration
        );
    }

    public function get(string $key, bool $required = false)
    {
        $settings = $this->toArray();

        if ($required && !isset($settings[$key])) {
            throw new ConfigurationException(
                sprintf('Missing configuration attribute `%s`', $key)
            );
        }

        return $settings[$key] ?? null;
    }

    public function toArray(): array
    {
        if (null === $this->type) {
            return self::$settings;
        }

        if (!isset(self::$settings[$this->type])) {
            throw new ConfigurationException(sprintf('Undefined configuration index `%s`', $this->type));
        }

        return self::$settings[$this->type];
    }
}
