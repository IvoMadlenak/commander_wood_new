<?php

namespace App\SymfonyCommands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \App\Core\Database;

class DbBuilderCommand extends Command
{
	/** @var string $dbBuildsDir */
    private $dbBuildsDir = '/data/dbBuilds/';

    /** @var $container */
    private $container;

    public function __construct(\Slim\Container $container)
    {
    	$this->container = $container;
    	parent::__construct();
    }

    protected function configure()
    {
        $this->setname('app:db-build')
        	->setDescription('Runs list of SQL commands.')
        	->setHelp('This command reads and runs all the SQL commands located in "data/dbBuilds" folder in alphabetical order.');
    }

    /**
     * Write number of lauched SQL query scripts.
     * @var InputInterface $input
     * @var OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $c  = $this->dbBuild();
        $qs = ($c == 1) ? 'query' : 'queries';

        $output->writeln('Database builder successfuly ran (' . $c . ') SQL ' . $qs . '.');
    }


    /**
     * Open folder defined in $dir variable, read all the files
     * and parse/run SQL queries stored in them.
     * @return int - number of SQL queries launched
     */
    private function dbBuild()
    {
    	$db = $this->container->get(Database::class);
        $dir   = PROJECT_DIR . $this->dbBuildsDir;
        $items = scandir($dir);
        $cntr  = 0;
        
        foreach ($items as $item) {
            if (in_array($item, ['.', '..']))
                continue;

            $sqls = explode(';', file_get_contents($dir . $item));

            foreach ($sqls as $sql) {
                $sql = trim($sql);
                if (strlen($sql)) {
                    $db->query($sql);
                    $cntr++;
                }
            }
        }

        return $cntr;
    }
}