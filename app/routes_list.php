<?php

use \App\Controllers\Customer;
use \App\Middleware\Auth;

/** @var \Slim\App $app */
$app->group('/api', function () use ($app) {
    $app->map(['GET', 'DELETE'], '', function ($request, $response, $args) {
    })->setName('api');

    $app->group('/v2', function() use($app) {
        $app->map(['GET', 'DELETE'], '', function ($request, $response, $args) {
        })->setName('apiv1');

        $app->group('/customer', function() use($app) {

            $app->post('/logout', Customer\CustomerController::class . ':setLogout');
            
            $app->post('/end-ride', Customer\CustomerController::class . ':setEndRide');

            $app->post('/start-ride', Customer\CustomerController::class . ':setStartRide');

            $app->get('/vehicles', Customer\CustomerController::class . ':getAllVehicles');
        })->add(new Auth($app->getContainer()));

        $app->get('/refresh-token', Customer\CustomerController::class . ':setRefreshToken');
        $app->post('/login', App\Controllers\Ride\RideController::class . ':login');

        /*$app->group('/test', function() use($app) {
            $app->map(['GET'], '', function ($request, $response, $args) {
            })->setName('apiv1test');

            $app->get('/print[/{text}]', function ($request, $response, $args) {
                print_r(" api test: " . $args['text']);
            })->setName('apiv1testprint');
        });*/
    });
});
