<?php
require PROJECT_DIR . '/vendor/autoload.php';

/*use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;*/
use Psr\Container\ContainerInterface;
use Slim\App;
use \App\Core\Database;

$appSettings   = new \App\Core\Settings();
$errorHandler  = new \App\ErrorHandler\ErrorHandler($appSettings->get('logger'));
$container     = new \Slim\Container($appSettings->toArray());
$container->offsetSet('logger', $errorHandler->getLogger());

//disable slim error handling - we will use Monolog library instead
unset($container['errorHandler'], $container['phpErrorHandler']);

// Database
$container->offsetSet(
    Database::class,
    function() use ($appSettings) {
        return new Database($appSettings->toArray()['database']);
    }
);

$container[App::class] = function (ContainerInterface $c) {
    $app = new App($c);

    // initialize routes and middlewares here
    /*$app->get('/', function (Request $request, Response $response, array $args) {
        $response->getBody()->write("Hello, man");

        return $response;
    });*/

    //file with list of all routes.
    require __DIR__ . "/routes_list.php";

    return $app;
};

return $container;
