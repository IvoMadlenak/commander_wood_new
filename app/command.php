<?php
/**
 * Created by PhpStorm.
 * User: adame
 * Date: 13. 4. 2018
 * Time: 12:43
 */
use Psr\Log\LoggerInterface;
use \App\Core\Database;


date_default_timezone_set('UTC');
error_reporting(E_ALL);
ini_set('display_errors', true);

define('PROJECT_DIR', dirname(__DIR__));

require __DIR__ . '/../vendor/autoload.php';

try {
    // assign neccessary variables (appSettings, errorHandler, container)
    $appSettings   = new \App\Core\Settings();
    $errorHandler  = new \App\ErrorHandler\ErrorHandler($appSettings->get('logger'));
    $container     = new \Slim\Container($appSettings->toArray());

    $container->offsetSet('logger', $errorHandler->getLogger());
    
    //register database container
    $container->offsetSet(
        Database::class,
        function() use ($appSettings) {
            return new Database($appSettings->toArray()['database']);
        }
    );

    // add Symfony scripts using built-in lazyloading
    $commandLoader = new Symfony\Component\Console\CommandLoader\FactoryCommandLoader([
        'app:db-build' => function() use ($container) {return new \App\SymfonyCommands\DbBuilderCommand($container);}
    ]);

    $application = new Symfony\Component\Console\Application();
    $application->setCommandLoader($commandLoader);
    $application->run();

} catch (\Exception $e) {
    $message = sprintf('Uncaught Exception %s: "%s" at %s line %s', get_class($e), $e->getMessage(), $e->getFile(), $e->getLine());

    if (!isset($logger)) {
        error_log($message);
    } else {
        $context = ['exception' => $e];
        $logger->error($message, $context);
    }

    echo $message . PHP_EOL;

    exit(1);
}
