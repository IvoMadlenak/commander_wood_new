<?php

namespace \ErrorHandler\Monolog\Processor;

final class ApiProcessor
{
    /** @var array */
    private $post;

    /** @var array */
    private $get;

    /** @var array */
    private $server;

    public function __construct(array $post = null, array $get = null, array $server = null)
    {
        $this->post = $post ?? $_POST;
        $this->get = $get ?? $_GET;
        $this->server = $server ?? $_SERVER;
    }

    public function __invoke(array $record)
    {
        $headers = $this->extractHeaders();

        $record['extra']['GET'] = json_encode($this->get);
        $record['extra']['POST'] = json_encode($this->post);
        $record['extra']['HTTP_HEADERS'] = json_encode($headers);

        return $record;
    }

    private function extractHeaders(): array
    {
        $headers = [];
        foreach ($this->server as $name => $value) {
            if ('HTTP_' !== substr($name, 0, 5)) {
                continue;
            }

            $key = substr($name, 5);
            $key = strtr(strtolower($key), '_', '-');
            $headers[$key] = $value;
        }

        return $headers;
    }
}
