<?php
declare(strict_types=1);

namespace App\ErrorHandler;

use App\ErrorHandler\Config\Config;
use Monolog\Processor\ApiProcessor;
use Exception\ConfigurationException;
use Monolog\Formatter\LineFormatter;
// use Monolog\Handler\GelfHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Monolog\Processor\WebProcessor;
use Psr\Log\LogLevel;

final class ErrorHandler
{
    private const ERROR_MAP = [
        E_ERROR             => LogLevel::CRITICAL,
        E_WARNING           => LogLevel::WARNING,
        E_PARSE             => LogLevel::ALERT,
        E_NOTICE            => LogLevel::NOTICE,
        E_CORE_ERROR        => LogLevel::CRITICAL,
        E_CORE_WARNING      => LogLevel::WARNING,
        E_COMPILE_ERROR     => LogLevel::ALERT,
        E_COMPILE_WARNING   => LogLevel::WARNING,
        E_USER_ERROR        => LogLevel::ERROR,
        E_USER_WARNING      => LogLevel::WARNING,
        E_USER_NOTICE       => LogLevel::NOTICE,
        E_STRICT            => LogLevel::NOTICE,
        E_RECOVERABLE_ERROR => LogLevel::ERROR,
        E_DEPRECATED        => LogLevel::NOTICE,
        E_USER_DEPRECATED   => LogLevel::NOTICE,
    ];

    private const HANDLER_GELF = 'gelf';
    private const HANDLER_STREAM = 'stream';

    /** @var array */
    private $config;

    /** @var string|null */
    private $facility;

    /** @var Logger */
    private $logger;

    private $handlers = [];

    public function __construct(array $config)
    {
        $this->config = $config;

        $this->logger = new \Monolog\Logger($this->getFacility());

        $this->logger->pushProcessor(new UidProcessor());

        if ('cli' != php_sapi_name()) {
            $this->logger->pushProcessor(new WebProcessor());
            // $this->logger->pushProcessor(new ApiProcessor());
        }

        //register handlers
        // $this->setGelfHandler();
        $this->setStreamHandler();

        if (empty($this->handlers)) {
            throw new ConfigurationException('No error handler enabled');
        }

        \Monolog\ErrorHandler::register($this->logger, self::ERROR_MAP, null, LogLevel::ALERT);
    }

    private function getHandlerConfig(string $name): Config
    {
        return new Config($name, $this->config['handlers'][$name]);
    }

    private function setGelfHandler(): void
    {
        $config = $this->getHandlerConfig(self::HANDLER_GELF);

        if ($config->getEnabled()) {
            $transport = new TcpTransport(
                $config->getHost(),
                $config->getPort()
            );

            $publisher = new Publisher($transport);

            $this->logger->pushHandler(new GelfHandler($publisher, $this->getLogLevel()));

            $this->handlers[] = self::HANDLER_GELF;
        }
    }

    private function setStreamHandler(): void
    {
        $config = $this->getHandlerConfig(self::HANDLER_STREAM);

        if ($config->getEnabled()) {
            $handler = new StreamHandler($config->getPath(), $this->getLogLevel());

            // the default date format is "Y-m-d H:i:s"
            $dateFormat = 'Y-m-d H:i:s';

            // finally, create a formatter
            $formatter = new LineFormatter($config->getLineFormat(), $dateFormat);
            $formatter->includeStacktraces();

            $handler->setFormatter($formatter);
            $this->logger->pushHandler($handler);

            $this->handlers[] = self::HANDLER_STREAM;
        }
    }

    private function getFacility(): string
    {
        if (null === $this->facility) {
            if (empty($this->config['basename'])) {
                throw new ConfigurationException('Undefined logger basename');
            }

            $this->facility = $this->config['basename'];
            if ('cli' == php_sapi_name()) {
                $this->facility .= '_cli';
            }

            if (defined('APPLICATION_ENV')) {
                $this->facility .= '_' . APPLICATION_ENV;
            }
        }

        return $this->facility;
    }

    private function getLogLevel(): string
    {
        if (empty($this->config['loglevel'])) {
            throw new ConfigurationException('Undefined log level');
        }

        return $this->config['loglevel'];
    }

    public function getLogger(): Logger
    {
        return $this->logger;
    }
}
