<?php
declare(strict_types=1);

namespace App\ErrorHandler\Config;

use \Exception\ConfigurationException;

final class Config
{
    /** @var string */
    private $name;

    /** @var array */
    private $attributes = [];

    public function __construct(string $name, array $attributes)
    {
        $this->name = $name;

        if (empty($attributes)) {
            throw new ConfigurationException(sprintf('Empty config for error handler %s', $name));
        }

        $this->attributes = $attributes;
    }

    /**
     * @param string $name
     * @param bool $required
     * @throws ConfigurationException
     * @return mixed
     */
    private function getAttribute(string $name, bool $required = true)
    {
        if (isset($this->attributes[$name])) {
            return $this->attributes[$name];
        }

        if ($required) {
            throw new ConfigurationException(sprintf('Error handler `%s` property `%s` not defined', $this->name, $name));
        }
    }

    public function getHost(): string
    {
        return $this->getAttribute('host');
    }

    public function getPort(): int
    {
        return (int) $this->getAttribute('port');
    }

    public function getPath(): string
    {
        return $this->getAttribute('path');
    }

    public function getLineFormat(): string
    {
        return $this->getAttribute('format');
    }

    public function getEnabled(): bool
    {
        return $this->getAttribute('enabled');
    }
}
