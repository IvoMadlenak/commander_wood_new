<?php
declare(strict_types=1);

namespace App\Models\Ride;

use App\Core\Database;

class RideModel
{
    /** @var $db \App\Core\Database */
    private $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function insertLogin($arr_data)
    {
        $id=intval($arr_data['id']);
        $auth_token=strval($arr_data['auth_token']);
        $device_id=intval($arr_data['device_id']);
        $auth_token_time=date('Y-m-d H:i:s', strtotime('+ 3 hours'));
        $refresh_token=strval($arr_data['refresh_token']);
        $ref_token_time='2033-01-01 00:00:00';

        $sql = "INSERT INTO `user_login_new` (`users_id`, `auth_token`, `device_id`, `auth_token_time`, `ref_token`, `ref_token_time`)
        VALUES (:id, :auth_token, :device_id, :auth_token_time, :refresh_token, :ref_token_time)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->bindValue(':auth_token', $auth_token, \PDO::PARAM_INT);
        $stmt->bindValue(':device_id', $device_id, \PDO::PARAM_INT);
        $stmt->bindValue(':auth_token_time', $auth_token_time, \PDO::PARAM_STR);
        $stmt->bindValue(':refresh_token', $refresh_token, \PDO::PARAM_INT);
        $stmt->bindValue(':ref_token_time', $ref_token_time, \PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll();
 
    }

    public function createNewRide($arr_data)
    {
        $user_id=intval($arr_data['user_id']);
        $object_id=intval($arr_data['object_id']);
        $customer_id=intval($arr_data['customer_id']);
        $created_at=date('Y-m-d H:i:s');
        $required_fields=strval($arr_data['required_fields']);
        $send_info=1;

        $sql = "INSERT INTO `rides_new` (`user_id`,`object_id`,`customer_id`,`created_at`, `required_fields`, `send_info`)
        VALUES (:user_id, :object_id, :customer_id, :created_at, :required_fields, :send_info)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':user_id', $user_id, \PDO::PARAM_INT);
        $stmt->bindValue(':object_id', $object_id, \PDO::PARAM_INT);
        $stmt->bindValue(':customer_id', $customer_id, \PDO::PARAM_INT);
        $stmt->bindValue(':created_at', $created_at, \PDO::PARAM_INT);
        $stmt->bindValue(':required_fields', $required_fields, \PDO::PARAM_INT);
        $stmt->bindValue(':send_info', $send_info, \PDO::PARAM_INT);
        $stmt->execute();

        $sql = "SELECT 
        r.id,
        o.objekt_id vehicleId,
        o.objekt_nazev vehicleName,
        op.objekt_polozky_hodnota vehicleRegistrationPlate,
        r.created_at,
        r.finished_at,
        r.required_fields,
        c.id customerId,
        c.name customerName
        FROM gpscom.objekt o
        LEFT JOIN gpscom.objekt_polozky op on op.objekt_id=o.objekt_id
        LEFT JOIN rides_new r on r.object_id=o.objekt_id
        LEFT JOIN customers_new c on r.customer_id=c.id
        WHERE o.objekt_id= :object_id AND r.finished_at IS NULL AND op.objekt_polozka_id=1 
        ORDER BY id desc LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':object_id', $object_id, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function isInsertDeviceId(int $device_id)
    {
        $sql = "SELECT *
        FROM user_login_new l
        WHERE l.device_id = :device_id";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':device_id', $device_id, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function isActiveRide(int $users_id)
    {
        $sql = "SELECT 
        r.id,
        o.objekt_id vehicleId,
        o.objekt_nazev vehicleName,
        op.objekt_polozky_hodnota vehicleRegistrationPlate,
        r.created_at,
        r.finished_at,
        r.required_fields,
        c.id customerId,
        c.name customerName
        FROM gpscom.objekt o
        LEFT JOIN gpscom.objekt_polozky op on op.objekt_id=o.objekt_id
        LEFT JOIN rides_new r on r.object_id=o.objekt_id
        LEFT JOIN customers_new c on r.customer_id=c.id
        WHERE r.user_id= :users_id AND r.finished_at IS NULL AND op.objekt_polozka_id=1 
        ORDER BY id desc ";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':users_id', $users_id, \PDO::PARAM_INT);
        $stmt->execute();

        //return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $stmt->fetchAll();
    }
    
    public function getUserPassword(string $objectId)
    {
        $sql = "SELECT 
        u.id,
        u.login_name,
        u.password_name
        FROM user_new u
        WHERE u.login_name = :object_id AND active=1";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':object_id', $objectId, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function getUserPasswordAuth(string $auth_token)
    {
        $sql = "SELECT * FROM
        user_login_new l LEFT JOIN 
        user_new u on l.users_id=u.id 
        WHERE l.auth_token= :auth_token";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':auth_token', $auth_token, \PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function getVehicles(int $userId)
    {
        $sql = "SELECT 
        v.object_id vehicleId,
        o.objekt_nazev vehicleName,
        op.objekt_polozky_hodnota vehicleRegistrationPlate,
        v.visible
        FROM vehicle_list_new v
        LEFT JOIN gpscom.objekt_polozky op on op.objekt_id=v.object_id
        LEFT JOIN gpscom.objekt o on o.objekt_id=v.object_id
        WHERE v.user_id=:usr_id AND op.objekt_polozka_id=1";

        $stmt = $this->db->prepare($sql);
        
        $stmt->bindValue(':usr_id', $userId, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getCustomer()
    {
        $sql = "SELECT 
        c.id,
        c.name,
        c.priority,
        c.required_fields
        FROM customers_new c
        WHERE active=1 
        order by priority";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function updateEndRide(int $ride)
    {
        $date_now=date('Y-m-d H:i:s');
        $sql = "UPDATE rides_new SET finished_at='$date_now' WHERE finished_at IS NULL AND id=:ride";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':ride', $ride, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->rowCount();
    }

    
    public function updateAuthToken(string $new_auth_token, string $ref_token)
    {
        $sql = "SELECT *
        FROM user_login_new 
        WHERE ref_token=:ref_token";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':ref_token', $ref_token, \PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->rowCount()==0){
            return -1;
        }else{
            $auth_token_time=date('Y-m-d H:i:s', strtotime('+ 3 hours'));
            $sql = "UPDATE user_login_new SET auth_token=:new_auth_token, auth_token_time=:auth_token_time WHERE ref_token=:ref_token";

            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':new_auth_token', $new_auth_token, \PDO::PARAM_STR);
            $stmt->bindValue(':auth_token_time', $auth_token_time, \PDO::PARAM_INT);
            $stmt->bindValue(':ref_token', $ref_token, \PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->rowCount();
        }
        //return $stmt->fetchAll();
    }

    public function rand_string( $length ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars),0,$length);
    }

    public function deleteUserLogin(string $auth_token)
    {
        $sql = "DELETE FROM user_login_new 
        WHERE auth_token=:auth_token";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':auth_token', $auth_token, \PDO::PARAM_STR);
        $stmt->execute();
        
        return $stmt->rowCount();
    }

}