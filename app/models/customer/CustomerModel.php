<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: adame
 * Date: 13.9.2017
 * Time: 15:57
 */

namespace App\Models\Customer;

use App\Core\Database;

class CustomerModel
{
    /** @var $db \Infrastructure\Database\DbContainer */
    private $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function getAllCustomers($orderBy = 'id'): array
    {
        $res = $this->db
            ->select(['*'])
            ->from('customers')
            ->orderBy($orderBy, 'ASC')
            ->execute();

        return $res->fetchAll();
    }

    public function getAllActiveCustomers($orderBy = 'id'): array
    {
        $res = $this->db
            ->select(['*'])
            ->from('customers')
            ->where('active', '=' , 1)
            ->orderBy($orderBy, 'ASC')
            ->execute();

        return $res->fetchAll();
    }

    public function customerExists($id): bool
    {
        $stmt = $this->db
            ->select(['*'])
            ->from('customers')
            ->where('id', '=', $id)
            ->execute();

        return $stmt->rowCount > 0;
    }

    public function getById($id): array
    {
        $stmt = $this->db
            ->select()
            ->from('customers')
            ->where('id', '=', $id)
            ->execute();

        $customer = $stmt->fetch();
        if (!$customer)
            $customer = [];

        return $customer;
    }
}
