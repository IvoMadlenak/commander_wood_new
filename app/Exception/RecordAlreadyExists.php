<?php
declare(strict_types=1);

namespace \Exception;

class RecordAlreadyExists extends \InvalidArgumentException
{
}
