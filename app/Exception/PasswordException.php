<?php
declare(strict_types=1);

namespace \Exception;

class PasswordException extends \InvalidArgumentException
{
}
