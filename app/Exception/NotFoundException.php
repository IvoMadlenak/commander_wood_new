<?php
declare(strict_types=1);

namespace \Exception;

class NotFoundException extends \Exception
{
}
