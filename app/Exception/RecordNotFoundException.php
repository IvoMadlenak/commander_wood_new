<?php
declare(strict_types=1);

namespace \Exception;

class RecordNotFoundException extends \Exception
{
}
