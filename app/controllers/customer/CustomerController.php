<?php
declare(strict_types=1);

namespace App\Controllers\Customer;

use App\Core\Database;
use \App\Http\Controller\AbstractController;
use \App\Models\Customer\CustomerModel;
use \App\Models\Ride\RideModel;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\{Request, Response};

final class CustomerController extends AbstractController implements ICustomerController
{
    /** @var CustomerModel */
    private $customerModel;

    /** @var rideModel */
    private $rideModel;

    public function __invoke(Request $request, Response $response, callable $next = null) : Response {
        return $response;
    }

    public function __construct (ContainerInterface $container) {
        $connection = $container->get(Database::class);
        $this->customerModel = new CustomerModel($connection);
        $this->rideModel     = new RideModel($connection);
    }

    public function getAllVehicles(Request $request, Response $response): ResponseInterface
    {
        $aa=$request->getHeaders();
        $auth_token = implode(", ",$aa['HTTP_AUTHTOKEN']);
            
        $userPassword=$this->rideModel->getUserPasswordAuth($auth_token);
        $result=$this->rideModel->isActiveRide($userPassword[0]['users_id']);
        if(empty($result)){
            $result_json['customers']=$this->rideModel->getCustomer();
            $result_json['vehicles']=$this->rideModel->getVehicles($userPassword[0]['id']);
        }else{
            $result_json['ride']=$result;
        }
        
        return $response->withJson($result_json);
    }

    public function setStartRide(Request $request, Response $response): ResponseInterface
    {
        $status=200;
        $aa=$request->getHeaders();
        $auth_token = implode(", ",$aa['HTTP_AUTHTOKEN']);
        $login_post = $request->getParsedBody();

        $arr_data['object_id']=$login_post['vehicle_id'];
        $arr_data['customer_id']=$login_post['customer_id'];
        $userPassword=$this->rideModel->getUserPasswordAuth($auth_token);
        $arr_data['user_id']=$userPassword[0]['users_id'];
        $arr_data['required_fields']=$login_post['required_fields'];

        if(empty($this->rideModel->isActiveRide($arr_data['user_id']))){//zabezpeci aby sa nevytvorila nova jazda ak uz existuje stara neukoncena jazda
            $result_json['ride']=$this->rideModel->createNewRide($arr_data);
            
        }else{
            $result_json['message']="User has active ride.";
            $result_json['in_ride']=$this->rideModel->isActiveRide($arr_data['user_id']);
            $status=403;
        }
        
        return $response->withJson($result_json,$status);
    }

    public function setEndRide(Request $request, Response $response): ResponseInterface
    {
        $aa=$request->getHeaders();
        $auth_token = implode(", ",$aa['HTTP_AUTHTOKEN']);
        $login_post = $request->getParsedBody();
            
        $result=$this->rideModel->updateEndRide(intval($login_post['ride_id']));
        if($result==0){
            $result_json['message']="Finished_at of ride is not set";
            $status=401;
        }else{
            $result_json['message']="Ride was ended";
            $status=200;
        }
        return $response->withJson($result_json,$status);
    }

    public function setRefreshToken(Request $request, Response $response): ResponseInterface
    {
        $aa=$request->getHeaders();
        $ref_token = implode(", ",$aa['HTTP_REFTOKEN']);
        //$old_auth_token = implode(", ",$aa['HTTP_AUTHTOKEN']);
        //$auth_token
            $message="password invalidd";
            $new_auth_token= $this->rideModel->rand_string(20); 
            
            $result=$this->rideModel->updateAuthToken($new_auth_token,$ref_token);
            if($result==1){
                $result_json['auth_token']=$new_auth_token;
                $status=200;
            }elseif ($result==-1){
                $result_json['message']="Auth token is not updated because ref_token is not found";
                $status=401;

            }else{
                $result_json['message']="Auth token is not updated";
                $status=401;
            }
        return $response->withJson($result_json,$status);
    }

    public function setLogout(Request $request, Response $response): ResponseInterface
    {
        $aa=$request->getHeaders();
        $auth_token = implode(", ",$aa['HTTP_AUTHTOKEN']);
        //$auth_token
        
            $result=$this->rideModel->DeleteUserLogin($auth_token);
            if($result==1){
                $result_json['message']="user is logged out";
                $status=200;
            }else{
                $result_json['message']="user is not logged out";
                $status=401;
            }
            /*$result_json['customer']=$this->rideModel->getCustomer();
            $result_json['vehicles']=$this->rideModel->getVehicles($userPassword[0]['id']);*/
    
        
        return $response->withJson($result_json,$status);
    }


    
}
