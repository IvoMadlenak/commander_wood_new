<?php
declare(strict_types=1);

namespace App\Controllers\Customer;

use Slim\Http\Request;
use Slim\Http\Response;

interface ICustomerController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param callable|null $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next = null): Response;
}
