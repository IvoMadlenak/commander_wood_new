<?php
declare(strict_types=1);

namespace App\Controllers\Ride;

use App\Core\Database;
use \App\Http\Controller\AbstractController;
use \App\Models\Ride\RideModel;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\{Request, Response};

final class RideController extends AbstractController
{
    /** @var rideModel */
    private $rideModel;

    public function __invoke(Request $request, Response $response, callable $next = null) : Response {
        return $response;
    }

    public function __construct (ContainerInterface $container) {
        $connection = $container->get(Database::class);
        $this->rideModel = new RideModel($connection);
    }

    public function login(Request $request, Response $response): ResponseInterface
    {
        $result_json['message']="Password incorrect";
        
        //$device_id=$request->getAttribute('device_id');
        $login_post = $request->getParsedBody();
        $login_name=$login_post['name'];
        $device_id=$login_post['device_id'];
        $userPassword=$this->rideModel->getUserPassword($login_name);
        if(!isset($userPassword)){
            $result_json['message']="Invalid name";
        }
        $send_heslo=$login_post['password'];
        if(!empty($userPassword)){
            $heslo=$userPassword[0]['password_name'];
        }else{
            $heslo="";
            //$result_json['message']="name incorrect";
        }
        $status=401;
        if (password_verify($send_heslo, $heslo)) {
            $status=200;
            $arr['id'] = ($userPassword[0]['id']); 
            $arr['auth_token'] = $this->rideModel->rand_string(20); 
            $arr['refresh_token'] = $this->rideModel->rand_string(20);
            unset($result_json['message']);
            $result_json['customers']=$this->rideModel->getCustomer();
            $result_json['vehicles']=$this->rideModel->getVehicles($userPassword[0]['id']);
            $arr['device_id'] = $device_id;
            $date_now=date('Y-m-d H:i:s');

            if(empty($this->rideModel->isInsertDeviceId(intval($device_id)))){
                $message = $this->rideModel->insertLogin($arr);
                $dataDevice=$this->rideModel->isInsertDeviceId(intval($device_id));
                $result_json['auth_token']=$dataDevice[0]['auth_token'];
                $result_json['refresh_token']=$dataDevice[0]['ref_token'];
                $dataDevice2=$this->rideModel->isActiveRide(($dataDevice[0]['users_id']));
                if(!empty($dataDevice2)){
                    unset($result_json);
                    $result_json['ride']=$dataDevice2;
                    $result_json['auth_token']=$dataDevice[0]['auth_token'];
                    $result_json['refresh_token']=$dataDevice[0]['ref_token'];
                }
            }else{//ak je uzivatel prihlaseny na device_id
                $dataDevice=$this->rideModel->isInsertDeviceId(intval($device_id));
                $dataDevice2=$this->rideModel->isActiveRide(($dataDevice[0]['users_id']));
                $result_json['auth_token']=$arr['auth_token'];
                
                //$result_json['refresh_token']=$arr['ref_token'];
                if(!empty($dataDevice2)){
                    unset($result_json);
                    $result_json['ride']=$dataDevice2;
                    $result_json['auth_token']=$arr['auth_token'];
                }
                $result_update=$this->rideModel->updateAuthToken($arr['auth_token'],$dataDevice[0]['ref_token']);
                if($result_update==0){
                    $result_json['message']="Cannot update auth_token.";
                    $status=403;
                }
            }
            
        } else {
            $result_json['message']="Incorrect name or password";
        }
        
        return $response->withJson($result_json,$status);
    }
}
