<?php
declare(strict_types=1);

namespace Infrastructure\Core\Http\Request\Validator\UserSettings;

final class LeftMenuOrderValidator extends AbstractValidator
{
    private const KEY_ITEM_ID_LIST = 'item_id_list';

    /** @var bool */
    private $valid;

    public function isValid(): bool
    {
        if (null === $this->valid) {
            $idList = $this->getAttribute(self::KEY_ITEM_ID_LIST);

            array_filter($idList, function (int $x) { //strict typing, it is not necessary to throw exception for invalid values
                return is_int($x);
            });

            $this->valid = true;
        }

        return $this->valid;
    }
}
