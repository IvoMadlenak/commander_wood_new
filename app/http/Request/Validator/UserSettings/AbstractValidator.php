<?php
declare(strict_types=1);

namespace Infrastructure\Core\Http\Request\Validator\UserSettings;

abstract class AbstractValidator implements IUserSettingsValidator
{
    /** @var string */
    private $key;

    /** @var array */
    private $data;

    public function __construct(string $key, array $data)
    {
        $this->key = $key;
        $this->data = $data;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getSettings(): array
    {
        return $this->data;
    }

    public function getAttribute(string $name, bool $required = true)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        if ($required) {
            throw new \InvalidArgumentException(
              sprintf('Undefined %s attribute %s', $this->key, $name)
            );
        }

        return null;
    }
}
