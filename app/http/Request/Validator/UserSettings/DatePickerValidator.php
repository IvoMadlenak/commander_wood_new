<?php
declare(strict_types=1);

namespace Infrastructure\Core\Http\Request\Validator\UserSettings;

final class DatePickerValidator extends AbstractValidator
{
    private const DATE_MIN_KEY = 'dateLow';
    private const DATE_MAX_KEY = 'dateHigh';
    private const INTERVAL_KEY = 'interval';

    private const TODAY = 'TODAY';
    private const YESTERDAY = 'YESTERDAY';
    private const LAST_WEEK = 'LAST_WEEK';
    private const LAST_MONTH = 'LAST_MONTH';
    private const THIS_MONTH = 'THIS_MONTH';
    private const LAST_YEAR = 'LAST_YEAR';

    private const INTERVAL_VALUES = [
        self::TODAY,
        self::YESTERDAY,
        self::LAST_WEEK,
        self::THIS_MONTH,
        self::LAST_MONTH,
        self::LAST_YEAR,
    ];

    /** @var bool */
    private $valid;

    public function isValid(): bool
    {
        if (null === $this->valid) {
            /**
             * there are two possible options:
             * a) interval defined by "word" or
             * b) interval defined by 2 dates - min and max
             * higher priority has a) "interval" definition
             */
            $interval = $this->getAttribute(self::INTERVAL_KEY, false);

            if (empty($interval)) {
                //both dates MUST be defined
                $min = $this->getAttribute(self::DATE_MIN_KEY, true);
                $max = $this->getAttribute(self::DATE_MAX_KEY, true);

                $minDate = new \DateTime($min);
                $maxDate = new \DateTime($max);

                if ($minDate > $maxDate) {
                    throw new \InvalidArgumentException(
                        sprintf('%s cannot be higher than %s', self::DATE_MIN_KEY, self::DATE_MAX_KEY)
                    );
                }
            } else {
                if (false === array_search($interval, self::INTERVAL_VALUES)) {
                    throw new \InvalidArgumentException(
                        sprintf('Uknown interval value %s', $interval)
                    );
                }
            }

            $this->valid = true;
        }

        return $this->valid;
    }
}
