<?php
declare(strict_types=1);

namespace Infrastructure\Core\Http\Request\Validator\UserSettings;

use Domain\Model\UserSettings\UserSettings;
use Slim\Http\Request;

final class UserSettingsValidatorFactory
{
    private const VALIDATORS = [
        UserSettings::KEY_TYPE_DATE_PICKER => DatePickerValidator::class,
        UserSettings::KEY_TYPE_LEFT_MENU => LeftMenuOrderValidator::class,
    ];

    public function getSettingsKey(array $args): string
    {
        $key = $args['key'] ?? null;

        if (!$key) {
            throw new \InvalidArgumentException('Undefined key attribute in args');
        }

        if (!isset(self::VALIDATORS[$key])) {
            throw new \RuntimeException(sprintf('Unknown settings key `%s`', $key));
        }

        return (string) $key;
    }

    public function getFromRequest(Request $request, array $args): IUserSettingsValidator
    {
        $key = $this->getSettingsKey($args);

        $body = (string) $request->getBody(); //convert stream to string, fix for codeception tests

        $data = json_decode($body, true);

        if (!$data || !is_array($data)) {
            throw new \InvalidArgumentException('Invalid settings json');
        }

        $class = self::VALIDATORS[$key];

        /** @var IUserSettingsValidator $validator */
        $validator = new $class($key, $data);
        if (!$validator->isValid()) {
            throw new \InvalidArgumentException('Invalid user settings json');
        }

        return $validator;
    }
}
