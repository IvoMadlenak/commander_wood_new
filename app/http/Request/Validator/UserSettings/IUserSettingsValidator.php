<?php
declare(strict_types=1);

namespace Infrastructure\Core\Http\Request\Validator\UserSettings;

interface IUserSettingsValidator
{
    public function getKey(): string;

    public function getSettings(): array;

    public function isValid(): bool;
}
