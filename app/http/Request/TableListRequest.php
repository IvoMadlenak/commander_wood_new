<?php
/**
 * Created by PhpStorm.
 * User: adame
 * Date: 31. 10. 2018
 * Time: 13:22
 */

namespace Infrastructure\Core\Http\Request;

use Domain\Model\TableList\Filter;
use Domain\Model\TableList\Paging;
use Domain\Model\TableList\Sorting;
use Domain\Model\TableList\TableListSettings;
use Slim\Http\Request;

final class TableListRequest
{
    /** @var Request */
    private $request;

    /** @var Paging */
    private $paging;

    /** @var Filter */
    private $filter;

    /** @var Sorting */
    private $sorting;

    /** @var array */
    private $columns;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function createFromRequest(Request $request): self
    {
        return new self($request);
    }

    public function validate(): TableListSettings
    {
        $pagingSettings = $this->request->getAttribute('paging');
        $sortSettings = $this->request->getAttribute('sort');
        $filterSettings = $this->request->getAttribute('filter');

        $this->paging = new Paging($pagingSettings);
        $this->filter = new Filter(key($filterSettings), $filterSettings[0]);
        $this->sorting = new Sorting();

        foreach ($sortSettings as $type => $value) {
            $this->sorting->add($type, $value);
        }

        return new TableListSettings($this->paging, $this->sorting, $this->filter, $this->columns);
    }
}
