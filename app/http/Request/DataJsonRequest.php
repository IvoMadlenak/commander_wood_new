<?php
declare(strict_types=1);

namespace Infrastructure\Core\Http\Request;

use Slim\Http\Request;

final class DataJsonRequest
{
    private $data;

    private function __construct(array $data)
    {
        $this->data = $data;
    }

    public static function createFromRequest(Request $request): self
    {
        $body = $request->getParsedBody();

        if (!isset($body['data'])) {
            throw new \InvalidArgumentException('Undefined request `data` attribute');
        }

        $data = json_decode($body['data'], true);
        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid json');
        }

        return new self($data);
    }

    public function getAttribute(string $name, bool $required = false)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        if ($required) {
            throw new \InvalidArgumentException(
              sprintf('Attribute `%s` required in json', $name)
            );
        }

        return null;
    }
}
