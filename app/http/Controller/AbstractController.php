<?php
declare(strict_types=1);

namespace App\Http\Controller;

use \App\Http\Response;

abstract class AbstractController
{
    protected function showError(string $message, int $httpStatus = 503): Response\CustomResponse
    {
        $response = [
            'status' => 'error',
            'message' => $message,
        ];

        return $this->response($response, $httpStatus);
    }

    protected function response(array $data, $httpStatus = 200): Response\CustomResponse
    {
        $resp = new Response\CustomResponse($httpStatus);
        return $resp->withJson($data);
    }
}
