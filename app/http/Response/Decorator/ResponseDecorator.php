<?php
declare(strict_types=1);

namespace Infrastructure\Core\Http\Response\Decorator;

use Slim\Http\Response;

final class ResponseDecorator implements \JsonSerializable
{
    public const STATUS_OK = 200;
    public const STATUS_NOT_FOUND = 400;

    /**
     * @var array|null
     */
    private $data;

    /**
     * @var array|null
     */
    private $errors;

    /**
     * @var int
     */
    private $code;

    /**
     * ResponseDecorator constructor.
     * @param mixed|null $data
     * @param array|null $errors
     * @param int $code
     */
    private function __construct($data = null, array $errors = null, int $code = self::STATUS_OK)
    {
        $this->data = $data;
        $this->errors = $errors;
        $this->code = $code;
    }

    public function jsonSerialize(): array
    {
        return [
            'result' => false !== $this->data,
            'data' => $this->data,
            'errors' => $this->errors,
        ];
    }

    public static function error(string $message, string $shortMessage = 'APPLICATION_ERROR', int $code = self::STATUS_NOT_FOUND, array $trace = []): self
    {
        $error = [
            'msg' => $shortMessage,
            'code' => $code,
            'details' => $message,
            'trace' => $trace,
        ];

        return new self([], [$error], $code);
    }

    public static function create($data): self
    {
        return new self($data, []);
    }

    public function withResponse(Response $response): Response
    {
        return $response->withJson($this, $this->code);
    }
}
