# Transport Dispatcher API
----------

## Documentation
https://commander.atlassian.net/wiki/spaces/CCC/pages/562364420/Backend+pro+mobilku+-+draft

## Development prerequisities
- docker - please see https://docs.docker.com/install
- docker-compose - https://docs.docker.com/compose/install/

## Start docker containers
Please type following commands in your terminal:
```bash
cd <project_dir>
docker-compose up -d
```

## Project init
```bash
cd <project_dir>
cp configs/settings.local.php.dist configs/settings.local.php
docker-compose exec --user www-data app composer install
```
Then test http://localhost:10098 in your browser.